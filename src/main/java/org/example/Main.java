package org.example;

public class Main {
    public static final String PROPERTIES = "secondPrProp.properties";

    // add
    public static void main(String[] args) {
        FileReader fileR = new FileReader();
        String min = fileR.readFile(PROPERTIES).getProperty("min");
        String max = fileR.readFile(PROPERTIES).getProperty("max");
        String increment = fileR.readFile(PROPERTIES).getProperty("increment");
        String dataType = fileR.readDataType();
        Calculator calculator = new Calculator();
        calculator.createMultiplyTable(min, max, increment, dataType);
    }
}