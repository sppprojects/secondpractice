package org.example;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.math.BigDecimal;
import java.math.BigInteger;
import java.math.RoundingMode;
import java.util.ArrayList;
import java.util.Collections;

import static org.example.MyValidatorUtil.*;

public class Calculator {
    private static final Logger logger = LoggerFactory.getLogger(Calculator.class);
    public static final String MINIMUM_GREATER_THAN_MAXIMUM = "Minimum is greater than maximum value and increment is positive.";

    void createMultiplyTable(String min, String max, String increment, String dataType) {
        if ("default".equals(defineType(dataType))) {
            dataType = "int";
        }
        try {
            if ("double".equals(defineType(dataType))) {
                workWithDouble(min, max, increment, dataType);
            } else {
                workWithInteger(min, max, increment, dataType);
            }
        } catch (IllegalArgumentException e) {
            logger.error(e.getMessage(), e);
        }

    }

    protected void workWithInteger(String min, String max,
                                   String increment, String dataType) throws IllegalArgumentException {
        BigInteger minBig = new BigInteger(min);
        BigInteger maxBig = new BigInteger(max);
        BigInteger incrementBig = new BigInteger(increment);
        if (isDataValid(minBig, maxBig, dataType)) {
            if (isMinGreaterMaxAndIncrementNegative(minBig, maxBig, incrementBig)) {
                BigInteger[] adjustedData = validateAndAdjustData(minBig, maxBig, incrementBig);
                minBig = adjustedData[0];
                maxBig = adjustedData[1];
                incrementBig = adjustedData[2];
                printReverseResult(calculateLoop(minBig, maxBig, incrementBig, dataType));
            } else if (isMaxGreaterMinAndIncrementPositive(minBig, maxBig, incrementBig)) {
                printTheResult(calculateLoop(minBig, maxBig, incrementBig, dataType));
            } else {
                throw new IllegalArgumentException(MINIMUM_GREATER_THAN_MAXIMUM);
            }
        } else {
            throw new IllegalArgumentException("Value is bigger than maximum value of " + dataType);
        }
    }


    protected void workWithDouble(String min, String max,
                                  String increment, String dataType) throws IllegalArgumentException {
        BigDecimal minBig = new BigDecimal(min);
        BigDecimal maxBig = new BigDecimal(max);
        BigDecimal incrementBig = new BigDecimal(increment);
        if (isDataValid(minBig, maxBig, dataType)) {
            if (isMinGreaterMaxAndIncrementNegative(minBig, maxBig, incrementBig)) {
                BigDecimal[] adjustedData = validateAndAdjustData(minBig, maxBig, incrementBig);
                minBig = adjustedData[0];
                maxBig = adjustedData[1];
                incrementBig = adjustedData[2];
                printReverseResult(calculateLoop(minBig, maxBig, incrementBig, dataType));
            } else if (isMaxGreaterMinAndIncrementPositive(minBig, maxBig, incrementBig)) {
                printTheResult(calculateLoop(minBig, maxBig, incrementBig, dataType));
            } else {
                throw new IllegalArgumentException(MINIMUM_GREATER_THAN_MAXIMUM);
            }
        } else {
            throw new IllegalArgumentException("Value is bigger than maximum value of " + dataType);
        }
    }

    private boolean isMinGreaterMaxAndIncrementNegative(BigDecimal minBig,
                                                        BigDecimal maxBig, BigDecimal incrementBig) {
        return compareToDouble(minBig, maxBig) == 1
                && compareToDouble(incrementBig, BigDecimal.valueOf(0)) == -1;
    }

    private boolean isMinGreaterMaxAndIncrementNegative(BigInteger minBig,
                                                        BigInteger maxBig, BigInteger incrementBig) {
        return compareToInteger(minBig, maxBig) == 1
                && compareToInteger(incrementBig, BigInteger.valueOf(0)) == -1;
    }

    private boolean isMaxGreaterMinAndIncrementPositive(BigDecimal minBig,
                                                        BigDecimal maxBig, BigDecimal incrementBig) {
        return compareToDouble(minBig, maxBig) == -1
                && compareToDouble(incrementBig, BigDecimal.valueOf(0)) == 1;
    }

    private boolean isMaxGreaterMinAndIncrementPositive(BigInteger minBig,
                                                        BigInteger maxBig, BigInteger incrementBig) {
        return compareToInteger(minBig, maxBig) == -1
                && compareToInteger(incrementBig, BigInteger.valueOf(0)) == 1;
    }

    private void printReverseResult(ArrayList<String> strings) {
        Collections.reverse(strings);
        printTheResult(strings);
    }

    private void printTheResult(ArrayList<String> strings) {
        for (String line : strings) {
            logger.info(line);
        }
    }

    protected ArrayList<String> calculateLoop(BigDecimal min, BigDecimal max,
                                              BigDecimal increment, String dataType) throws IllegalArgumentException {
        ArrayList<String> resultsArray = new ArrayList<>();
        int scale = increment.scale() * 2;
        for (BigDecimal i = min; i.compareTo(max) <= 0; i = i.add(increment)) {
            for (BigDecimal j = min; j.compareTo(max) <= 0; j = j.add(increment)) {
                BigDecimal result = calculate(i, j);
                if (isResultValueValid(result, dataType)) {
                    String message = i + " * " + j + " = " + result.setScale(scale, RoundingMode.HALF_UP);
                    resultsArray.add(message);
                } else {
                    throw new IllegalArgumentException("Overflow: " + i + " * " + j
                            + " is greater than maximum value for " + dataType);
                }
            }
        }
        return resultsArray;
    }

    private static BigDecimal calculate(BigDecimal number1, BigDecimal number2) {
        return number1.multiply(number2);
    }

    protected ArrayList<String> calculateLoop(BigInteger min, BigInteger max,
                                              BigInteger increment, String dataType) throws IllegalArgumentException {
        ArrayList<String> resultsArray = new ArrayList<>();
        for (BigInteger i = min; i.compareTo(max) <= 0; i = i.add(increment)) {
            for (BigInteger j = min; j.compareTo(max) <= 0; j = j.add(increment)) {
                BigInteger result = calculate(i, j);
                if (isResultValueValid(result, dataType)) {
                    String message = i + " * " + j + " = " + result;
                    resultsArray.add(message);
                } else {
                    throw new IllegalArgumentException("Overflow: " + i + " * " + j
                            + " is greater than maximum value for " + dataType);
                }
            }
        }
        return resultsArray;
    }

    private BigInteger calculate(BigInteger number1, BigInteger number2) {
        return number1.multiply(number2);
    }
}
