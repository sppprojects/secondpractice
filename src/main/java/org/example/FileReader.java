package org.example;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.nio.charset.StandardCharsets;
import java.util.Properties;

public class FileReader {
    private static final Logger logger = LoggerFactory.getLogger(FileReader.class);

    protected Properties readFile(String fileName) {
        Properties appProps = new Properties();
        try {
            InputStream systemResourceAsStream = getClass().getClassLoader().getResourceAsStream(fileName);
            InputStreamReader reader = new InputStreamReader(systemResourceAsStream, StandardCharsets.UTF_8);
            appProps.load(reader);
        } catch (IOException e) {
            logger.error("Can't load file", e);
        }
        return appProps;
    }

    protected String readDataType() {
        String fileType = System.getProperty("type", "int");
        switch (fileType) {
            case "double":
                return "double";
            case "float":
                return "float";
            case "long":
                return "long";
            case "short":
                return "short";
            case "byte":
                return "byte";
            default:
                return "int";
        }
    }
}
