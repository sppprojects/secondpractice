package org.example;

import java.math.BigDecimal;
import java.math.BigInteger;

public class MyValidatorUtil {

    private MyValidatorUtil() {
        throw new IllegalStateException("Utility class");
    }

    public static final String SHORT = "short";
    public static final String FLOAT = "float";
    public static final String DOUBLE = "double";
    public static final String LONG = "long";
    public static final String BYTE = "byte";

    static String defineType(String dataType) {
        if (DOUBLE.equals(dataType) || FLOAT.equals(dataType)) {
            return DOUBLE;
        } else if (LONG.equals(dataType) || SHORT.equals(dataType) || BYTE.equals(dataType)) {
            return "int";
        }
        return "default";
    }

    static boolean isResultValueValid(BigInteger result, String dataType) {
        switch (dataType) {
            case LONG:
                return compareToInteger(result, BigInteger.valueOf(Long.MAX_VALUE)) != 1;
            case SHORT:
                return compareToInteger(result, BigInteger.valueOf(Short.MAX_VALUE)) != 1;
            case BYTE:
                return compareToInteger(result, BigInteger.valueOf(Byte.MAX_VALUE)) != 1;
            default:
                return compareToInteger(result, BigInteger.valueOf(Integer.MAX_VALUE)) != 1;
        }
    }

    static boolean isResultValueValid(BigDecimal result, String dataType) {
        if (dataType.equals(FLOAT)) {
            return compareToDouble(result, BigDecimal.valueOf(Float.MAX_VALUE)) != 1;
        }
        return compareToDouble(result, BigDecimal.valueOf(Double.MAX_VALUE)) != 1;
    }

    static boolean isDataValid(BigDecimal min, BigDecimal max,
                               String dataType) {
        if (dataType.equals(FLOAT)) {
            return compareToDouble(min, BigDecimal.valueOf(Float.MAX_VALUE)) != 1
                    && compareToDouble(max, BigDecimal.valueOf(Float.MAX_VALUE)) != 1;
        }
        return compareToDouble(min, BigDecimal.valueOf(Double.MAX_VALUE)) != 1
                && compareToDouble(max, BigDecimal.valueOf(Double.MAX_VALUE)) != 1;
    }

    static boolean isDataValid(BigInteger min, BigInteger max,
                               String dataType) {
        switch (dataType) {
            case LONG:
                return compareToInteger(min, BigInteger.valueOf(Long.MAX_VALUE)) != 1
                        && compareToInteger(max, BigInteger.valueOf(Long.MAX_VALUE)) != 1;
            case SHORT:
                return compareToInteger(min, BigInteger.valueOf(Short.MAX_VALUE)) != 1
                        && compareToInteger(max, BigInteger.valueOf(Short.MAX_VALUE)) != 1;
            case BYTE:
                return compareToInteger(min, BigInteger.valueOf(Byte.MAX_VALUE)) != 1
                        && compareToInteger(max, BigInteger.valueOf(Byte.MAX_VALUE)) != 1;
            default:
                return compareToInteger(min, BigInteger.valueOf(Integer.MAX_VALUE)) != 1
                        && compareToInteger(max, BigInteger.valueOf(Integer.MAX_VALUE)) != 1;
        }
    }

    static int compareToDouble(BigDecimal num1, BigDecimal num2) {
        return num1.compareTo(num2);
    }

    static int compareToInteger(BigInteger num1, BigInteger num2) {
        return num1.compareTo(num2);
    }

    static BigDecimal[] validateAndAdjustData(BigDecimal min, BigDecimal max,
                                              BigDecimal increment) {
        BigDecimal temp = min;
        min = max;
        max = temp;
        increment = increment.multiply(BigDecimal.valueOf(-1));
        return new BigDecimal[]{min, max, increment};
    }

    static BigInteger[] validateAndAdjustData(BigInteger min, BigInteger max,
                                              BigInteger increment) {
        BigInteger temp = min;
        min = max;
        max = temp;
        increment = increment.multiply(BigInteger.valueOf(-1));
        return new BigInteger[]{min, max, increment};
    }
}
