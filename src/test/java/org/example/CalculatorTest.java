package org.example;

import org.apache.commons.io.output.ByteArrayOutputStream;
import org.junit.jupiter.api.Test;

import java.io.PrintStream;
import java.math.BigInteger;

import static org.junit.jupiter.api.Assertions.*;

class CalculatorTest {
    @Test
    void testCalculateLoopBigDecimal() {
        String expectedOutput = "1 * 1 = 1\n" +
                "1 * 2 = 2\n" +
                "1 * 3 = 3\n" +
                "2 * 1 = 2\n" +
                "2 * 2 = 4\n" +
                "2 * 3 = 6\n" +
                "3 * 1 = 3\n" +
                "3 * 2 = 6\n" +
                "3 * 3 = 9\n";

        String actualOutput = getMultiplyTableOutput(1, 3, 1, "double")
                .replaceAll("\\r\\n", "\n") // Заміна CRLF на LF
                .replaceAll("\\r", "\n"); // Заміна CR на LF

        assertEquals(expectedOutput, actualOutput);
    }

    @Test
    void testCalculateLoopFloat() {
        float min = 1.0f;
        float max = 3.0f;
        float increment = 1.0f;

        String expectedOutput = "1.0 * 1.0 = 1.00\n" +
                "1.0 * 2.0 = 2.00\n" +
                "1.0 * 3.0 = 3.00\n" +
                "2.0 * 1.0 = 2.00\n" +
                "2.0 * 2.0 = 4.00\n" +
                "2.0 * 3.0 = 6.00\n" +
                "3.0 * 1.0 = 3.00\n" +
                "3.0 * 2.0 = 6.00\n" +
                "3.0 * 3.0 = 9.00\n";

        assertEquals(expectedOutput, getMultiplyTableOutput(min, max, increment, "float")
                .replaceAll("\\r\\n", "\n") // Заміна CRLF на LF
                .replaceAll("\\r", "\n")); // Заміна CR на LF
    }

    @Test
    void testCalculateLoopBigInteger() {
        String expectedOutput = "1 * 1 = 1\n" +
                "1 * 2 = 2\n" +
                "1 * 3 = 3\n" +
                "2 * 1 = 2\n" +
                "2 * 2 = 4\n" +
                "2 * 3 = 6\n" +
                "3 * 1 = 3\n" +
                "3 * 2 = 6\n" +
                "3 * 3 = 9\n";
        String[] args = {"1", "3", "1", "integer"};
        ByteArrayOutputStream outputStream = new ByteArrayOutputStream();
        System.setOut(new PrintStream(outputStream));
        Main.main(args);

        String actualOutput = outputStream.toString()
                .replaceAll("\\r\\n", "\n") // Заміна CRLF на LF
                .replaceAll("\\r", "\n");

        assertEquals(expectedOutput, actualOutput);
    }

    @Test
    void testCalculateLoopLong() {
        long min = 1L;
        long max = 3L;
        long increment = 1L;

        String expectedOutput = "1 * 1 = 1\n" +
                "1 * 2 = 2\n" +
                "1 * 3 = 3\n" +
                "2 * 1 = 2\n" +
                "2 * 2 = 4\n" +
                "2 * 3 = 6\n" +
                "3 * 1 = 3\n" +
                "3 * 2 = 6\n" +
                "3 * 3 = 9\n";

        assertEquals(expectedOutput, getMultiplyTableOutput(min, max, increment, "long")
                .replaceAll("\\r\\n", "\n") // Заміна CRLF на LF
                .replaceAll("\\r", "\n")); // Заміна CR на LF
    }

    @Test
    void testCalculateLoopShort() {
        short min = 1;
        short max = 3;
        short increment = 1;

        String expectedOutput = "1 * 1 = 1\n" +
                "1 * 2 = 2\n" +
                "1 * 3 = 3\n" +
                "2 * 1 = 2\n" +
                "2 * 2 = 4\n" +
                "2 * 3 = 6\n" +
                "3 * 1 = 3\n" +
                "3 * 2 = 6\n" +
                "3 * 3 = 9\n";

        assertEquals(expectedOutput, getMultiplyTableOutput(min, max, increment, "short")
                .replaceAll("\\r\\n", "\n") // Заміна CRLF на LF
                .replaceAll("\\r", "\n")); // Заміна CR на LF
    }

    @Test
    void testCalculateLoopByte() {
        BigInteger min = new BigInteger(String.valueOf(50));
        BigInteger max = new BigInteger(String.valueOf(55));
        BigInteger increment = new BigInteger(String.valueOf(1));
        Calculator calculator = new Calculator();

        assertThrows(IllegalArgumentException.class, () -> calculator.calculateLoop(min, max, increment, "byte"));
    }

    @Test
    void testCalculateLoopMinusIncrement() {
        String expectedOutput = "3 * 3 = 9\n" +
                "3 * 2 = 6\n" +
                "3 * 1 = 3\n" +
                "2 * 3 = 6\n" +
                "2 * 2 = 4\n" +
                "2 * 1 = 2\n" +
                "1 * 3 = 3\n" +
                "1 * 2 = 2\n" +
                "1 * 1 = 1\n";

        String actualOutput = getMultiplyTableOutput(3, 1, -1, "double")
                .replaceAll("\\r\\n", "\n") // Заміна CRLF на LF
                .replaceAll("\\r", "\n"); // Заміна CR на LF

        assertEquals(expectedOutput, actualOutput);
    }

    @Test
    void testIncorrectInputValuesInteger() {
        String min = String.valueOf(10);
        String max = String.valueOf(5);
        String increment = String.valueOf(1);
        Calculator calculator = new Calculator();
        assertThrows(IllegalArgumentException.class, () -> calculator.workWithInteger(min, max, increment, "int"));
    }

    @Test
    void testResultGreaterThanMaxValueByte() {
        String min = String.valueOf(100);
        String max = String.valueOf(100);
        String increment = String.valueOf(1);
        Calculator calculator = new Calculator();

        assertThrows(IllegalArgumentException.class, () -> calculator.workWithInteger(min, max, increment, "byte"));
    }

    @Test
    void testIncorrectInputValuesDouble() {
        String min = String.valueOf(Double.MAX_VALUE);
        String max = String.valueOf(Double.MAX_VALUE - 2);
        String increment = String.valueOf(1);
        Calculator calculator = new Calculator();
        assertThrows(IllegalArgumentException.class, () -> calculator.workWithDouble(min, max, increment, "double"));
    }

    @Test
    void testResultGreaterThanMaxValueDouble() {
        String min = String.valueOf(Double.MAX_VALUE / 2);
        String max = String.valueOf(Double.MAX_VALUE);
        String increment = String.valueOf(2000000000);
        Calculator calculator = new Calculator();

        assertThrows(IllegalArgumentException.class, () -> calculator.workWithDouble(min, max, increment, "double"));
    }

    @Test
    void testVerySmallValues() {
        String expectedOutput = "0.01 * 0.01 = 0.000100\n" +
                "0.01 * 0.013 = 0.000130\n" +
                "0.01 * 0.016 = 0.000160\n" +
                "0.013 * 0.01 = 0.000130\n" +
                "0.013 * 0.013 = 0.000169\n" +
                "0.013 * 0.016 = 0.000208\n" +
                "0.016 * 0.01 = 0.000160\n" +
                "0.016 * 0.013 = 0.000208\n" +
                "0.016 * 0.016 = 0.000256\n";

        String actualOutput = getMultiplyTableOutput(0.01, 0.017, 0.003, "double")
                .replaceAll("\\r\\n", "\n") // Заміна CRLF на LF
                .replaceAll("\\r", "\n"); // Заміна CR на LF

        assertEquals(expectedOutput, actualOutput);
    }

    @Test
    void testInvalidDataType() {
        String expectedOutput = "3 * 3 = 9\n" +
                "3 * 2 = 6\n" +
                "3 * 1 = 3\n" +
                "2 * 3 = 6\n" +
                "2 * 2 = 4\n" +
                "2 * 1 = 2\n" +
                "1 * 3 = 3\n" +
                "1 * 2 = 2\n" +
                "1 * 1 = 1\n";

        String actualOutput = getMultiplyTableOutput(3, 1, -1, "randomWord")
                .replaceAll("\\r\\n", "\n")
                .replaceAll("\\r", "\n");

        assertEquals(expectedOutput, actualOutput);
    }

    private String getMultiplyTableOutput(Number min, Number max, Number increment, String dataType) {
        // Перехоплюємо вивід належний методу calculateLoop
        ByteArrayOutputStream outputStream = new ByteArrayOutputStream();
        System.setOut(new PrintStream(outputStream));

        Calculator calculator = new Calculator();
        calculator.createMultiplyTable(String.valueOf(min), String.valueOf(max), String.valueOf(increment), dataType);

        // Повертаємо отриманий вивід як рядок
        return outputStream.toString();
    }
}